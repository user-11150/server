---
author: User-11150
---


# Redirect to https://github.com/user-11150/puel

# UEL
## 前言
> UEL是一个基于Python的一个解释型语言。
# 让我们开始使用他吧！
安装
```
python setup.py install
```
使用:
```
python -m uel 命令
```
也可以使用
```
uel 命令
```
## 链接
1. [Gitee](http://gitee.com/user-11150/server)
2. [Contributers](./docs/Contibuters.md)
3. [教学文档](docs/tutorial.md)