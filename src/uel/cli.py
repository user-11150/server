from sys import argv

from uel.core.Main import Main


def main() -> None:
    Main().main(argv)
