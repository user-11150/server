from uel.core.errors.UELBaseException import UELBaseException


class UELException(UELBaseException):
    pass
