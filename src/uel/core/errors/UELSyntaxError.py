from uel.core.errors.UELBuildtimeException import UELBuildtimeException


class UELSyntaxError(UELBuildtimeException):
    pass
