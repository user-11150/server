from uel.core.errors.runtime.UELRuntimeError import UELRuntimeError


class UELMakeObjectError(UELRuntimeError):
    pass
