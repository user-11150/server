from uel.core.errors.UELException import UELException


class UELRuntimeError(UELException):
    pass
