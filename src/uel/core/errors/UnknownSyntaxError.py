from uel.core.errors.UELBuildtimeException import UELBuildtimeException


class UnknownSyntaxError(UELBuildtimeException):
    pass
